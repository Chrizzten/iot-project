#!/bin/bash
INTERVAL=20
HTMLOUTPUT=/opt/iotproject/index.html
if [ $# -eq 1 ]
then
    if [ -d "$1" ] ; 
    then
        DIRECTORYTOLIST=$1
    else
        echo "Directory $1 not found." >&2
        exit 1
    fi
fi
shift

counter=0
while true ; do
    echo "<html><body>" > $HTMLOUTPUT
    echo "<h1>Mein Webserver</h1>" >> $HTMLOUTPUT
    date +%H:%M:%S $HTMLOUTPUT
    ls $DIRECTORYTOLIST >> $HTMLOUTPUT
    #For loop der die existenz des files zuerst prueft und dann anhaengt oder fehlermeldung
    for htmlsnippet in $*
    do
        if [ -r "$htmlsnippet" ] ; 
        then
            cat $htmlsnippet >> $HTMLOUTPUT 2>/dev/null
        else
            echo "File $htmlsnippet is not readable" >&2
        fi
    done
    echo "</body></html>" >> $HTMLOUTPUT
    sleep $INTERVAL
    counter=$(($counter+1))
done