#!/bin/bash

DIRECTORYTOLIST=/tmp # This is the default if no paramter S1 is given.

if [ $# -eq 1 ]
then
    if [ -d "$1" ] ; 
    then
        DIRECTORYTOLIST=$1
    else
        echo "directory $1 not found."
        exit 1
    fi
fi

ls $DIRECTORYTOLIST